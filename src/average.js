const average = (...args) => {
  let sum = 0;
  for (const i of args) {
    sum += i;
  }
  return sum / args.length;
};

export default average;
