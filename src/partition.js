const partition = (arr, value) => {
  const result1 = [];
  const result2 = [];
  for (let i = 0; i < arr.length; i += 1) {
    if (arr[i] < value) {
      result1.push(arr[i]);
    } else if (arr[i] > value) {
      result2.push(arr[i]);
    }
  }
  return [result1, result2];
};

export default partition;
