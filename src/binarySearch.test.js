import binarySearch from './binarySearch';

test('binarySearch', () => {
  expect(binarySearch([1, 2, 3, 4, 5, 6], 6)).toEqual(5);
  expect(binarySearch([1, 2, 3, 4, 5, 6], 7)).toEqual(-1);
  expect(binarySearch([1, 2, 3, 4, 5, 6, 7], 4)).toEqual(3);
});
