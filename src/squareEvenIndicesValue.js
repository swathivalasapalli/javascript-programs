const squareEvenIndicesValue = arr => {
  const result = [];
  for (let i = 0; i < arr.length; i += 1) {
    if (i % 2 === 0) {
      result.push(arr[i] * arr[i]);
    }
  }
  return result;
};

export default squareEvenIndicesValue;
