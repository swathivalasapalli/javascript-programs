// // @ts-nocheck
// const person1 = {
//   name: 'swathi',
//   sayName() {
//     console.log(this.name);
//   },
// };
// console.log('sayName' in person1);
// console.log(person1.hasOwnProperty('name'));
// console.log('toString' in person1);
// console.log(person1.hasOwnProperty('toString'));

// // ------ removing properties ------ //

// const person = {
//   name: 'swathi',
// };
// console.log('name' in person);

// delete person.name;
// console.log('name' in person);
// console.log(person.name);

// // ----- enumeration ----- //

// const employee = {
//   name: 'swathi',
// };
// console.log('name' in employee);
// console.log(employee.propertyIsEnumerable('name'));

// const properties = Object.keys(employee);
// console.log('length' in properties);
// console.log(properties.propertyIsEnumerable('length'));

// // ------- types of properties ------ //

// const employee1 = {
//   names: 'swathi',
//   get name() {
//     return this.names;
//   },
//   set name(value) {
//     this.names = value;
//   },
// };

// console.log(employee1.name);

// // ------- common attributes --------//

// const emp = {
//   name: 'swathi',
// };

// Object.defineProperty(emp, 'name', {
//   enumerable: false,
// });

// console.log('name' in emp);
// console.log(emp.propertyIsEnumerable('name'));

// const prop = Object.keys(emp);
// console.log(prop.length);

// Object.defineProperty(emp, 'name', {
//   configurable: false,
// });

// // delete emp.name; // delete property fails because property cannaot be changed(configurable:false)
// console.log('name' in emp);
// console.log(emp.name);

// // ------- Data property attributes --------- //

// const details = {};
// Object.defineProperty(details, 'name', {
//   value: 'swathi',
// });

// console.log(details);
// console.log('name' in details);
// console.log(details.propertyIsEnumerable('name'));
// // details.name = 'radhika'; --> we can only read the value
// console.log(details.name);

// // ---------- Accessor property attributes ---------//

// const empdetails = {
//   names: 'swathi',
//   get name() {
//     return this.names;
//   },
//   set name(value) {
//     this.names = value;
//   },
// };

// console.log(empdetails.name);

// // above code can be also written as follows
// const persondetails = {
//   name: 'swathi',
// };
// Object.defineProperty(person1, 'name', {
//   get() {
//     return this.names;
//   },
//   set(value) {
//     this.names = value;
//   },
//   enumerable: true,
//   configurable: true,
// });

// console.log(persondetails.name);

// // ---------- create a nonconfigurable, nonenumerable,nonwritable property -------//

// const detail = {
//   name: 'radhika',
// };
// Object.defineProperty(detail, 'name', {
//   get() {
//     return this.name;
//   },
// });

// console.log('name' in detail);
// console.log(detail.propertyIsEnumerable('name'));
// // detail.name = 'swathi' ---->  cannot set property name which has only getter

// // -------- Defining multiple properties --------- //

// const personinfo = {};
// // data property
// Object.defineProperties(personinfo, {
//   name: {
//     value: 'ramya',
//     enumerable: true,
//     configurable: true,
//     writable: true,
//   },
//   // accessor property
//   names: {
//     get name() {
//       return this.names;
//     },
//     set name(value) {
//       this.names = value;
//     },
//     enumerable: true,
//     configurable: true,
//   },
// });

// // ----------- Retrieving Property attributes ---------//

// const persondetail = {
//   name: 'ramya',
// };

// const descriptor = Object.getOwnPropertyDescriptor(persondetail, 'name');

// console.log(descriptor.enumerable);
// console.log(descriptor.configurable);
// console.log(descriptor.writable);
// console.log(descriptor.value);

// // -------- Preventing Object Modification -------//

// const info = {
//   name: 'radhika',
// };

// console.log(Object.isExtensible(info));

// Object.preventExtensions(info);
// console.log(Object.isExtensible(info));

// // info.sayName = () => console.log(this.name); ---> can't be added
// console.log('sayName' in info);

// // ------ Sealing objects -------- //

// const information = {
//   name: 'swathi',
// };

// console.log(Object.isExtensible(information));
// console.log(Object.isSealed(information));

// // information.sayName = () => console.log(this.name);---> fails
// console.log('sayName' in information);

// Object.seal(information);
// console.log(Object.isExtensible(information));
// console.log(Object.isSealed(information));

// information.name = 'ramya';
// console.log(information.name);

// // delete information.name ----> fails

// const desc = Object.getOwnPropertyDescriptor(information, 'name');
// console.log(desc.configurable);

// // ----- Freezing Objects ---------- //

// const p = {
//   name: 'sravani',
// };

// console.log(Object.isExtensible(p));
// console.log(Object.isSealed(p));
// console.log(Object.isFrozen(p));

// Object.freeze(p);
// console.log(Object.isExtensible(p));
// console.log(Object.isSealed(p));
// console.log(Object.isFrozen(p));

// // p.sayName = () => console.log(this.name); ----->fails
// console.log('sayName' in p);

// // p.name = 'sravanidande'; ---> fails property can't be changed
// console.log(p.name);

// // delete p.name;----> fails
// console.log('name' in p);
// console.log(p.name);

// // --------------- chapter 4 CONSTRUCTORS -------------------//
// function Person() {}

// const p1 = new Person();
// const p2 = new Person();

// console.log(p1 instanceof Person);
// console.log(p2 instanceof Person);
// console.log(p1.constructor === Person);
// console.log(p2.constructor === Person);

// // -------- Prototypes --------- //

// const book = {
//   title: 'The Principles of Object-oriented Javascript',
// };

// console.log('title' in book);
// console.log(book.hasOwnProperty('title'));
// console.log('hasOwnProperty' in book);
// console.log(book.hasOwnProperty('hasOwnProperty'));
// console.log(Object.prototype.hasOwnProperty('hasOwnProperty'));

// // -------- identifying prototype property ------- //

// const hasPrototypeProperty = (object, name) =>
//   name in object && !object.hasOwnProperty(name);

// console.log(hasPrototypeProperty(book, 'title'));
// console.log(hasPrototypeProperty(book, 'hasOwnProperty'));

// // -------- The [[Prototype]] Property -------- //

// const object = {};
// const prototype = Object.getPrototypeOf(object);
// console.log(prototype === Object.prototype);

// // You can also test to see if one object is a prototype for another by using isPrototypeOf method

// const obj = {};
// console.log(Object.prototype.isPrototypeOf(obj));

// const o = {};
// console.log(o.toString());

// o.toString = () => '[object Custom]';
// console.log(o.toString());

// delete o.toString; // ---> works only on own properties
// console.log(o.toString());

// // ------- using prototypes with constructors ---- //
// class Detail {
//   constructor(name) {
//     this.name = name;
//   }

//   sayName() {
//     console.log(this.name);
//   }
// }

// const d1 = new Detail('hello');
// const d2 = new Detail('hai');
// console.log(d1.name);
// console.log(d2.name);
// d1.sayName();
// d2.sayName();

// class Info {
//   constructor(name) {
//     this.name = name;
//   }
// }
// Info.prototype.sayName = () => {
//   console.log(this.name);
// };

// Info.prototype.favorites = [];
// const i1 = new Info('swathi');
// const i2 = new Info('sravani');
// i1.favorites.push('javascript');
// i2.favorites.push('react');
// console.log(i1.favorites);
// console.log(i2.favorites);

export const all = (arr, fn) => arr.every(fn);
console.log(all([4, 2, 3], x => x > 1));

export const drop = (arr, n = 1) => arr.slice(n);

console.log(drop([1, 2, 3]));
console.log(drop([1, 2, 3], 2));

export const dropRight = (arr, n = 1) => arr.slice(0, -n);

console.log(dropRight([1, 2, 3]));
console.log(dropRight([1, 2, 3], 2));

export const dropRightWhile = (arr, func) => {
  while (arr.length > 0 && !func(arr[arr.length - 1])) arr = arr.slice(0, -1);
  return arr;
};

console.log(dropRightWhile([1, 2, 3, 4], n => n < 3));

export const dropWhile = (arr, func) => {
  while (arr.length > 0 && !func(arr[0])) arr = arr.slice(1);
  return arr;
};

console.log(dropWhile([1, 2, 3, 4], n => n >= 3));
export default all;

export const everyNth = (arr, nth) => arr.filter((e, i) => i % nth === nth - 1);

console.log(everyNth([1, 2, 3, 4, 5, 6], 2));

export const filterFalsy = arr => arr.filter(Boolean);

console.log(filterFalsy(['', true, {}, false, 'sample', 1, 0]));

export const filterNonUnique = arr =>
  arr.filter(i => arr.indexOf(i) === arr.lastIndexOf(i));

console.log(filterNonUnique([1, 2, 2, 3, 4, 4, 5]));

export const filterNonUniqueBy = (arr, fn) =>
  arr.filter((v, i) => arr.every((x, j) => (i === j) === fn(v, x, i, j)));

console.log(
  [
    { id: 0, value: 'a' },
    { id: 1, value: 'b' },
    { id: 2, value: 'c' },
    { id: 1, value: 'd' },
    { id: 0, value: 'e' },
  ],
  (a, b) => a.id === b.id,
);

export const findLast = (arr, fn) => arr.filter(fn).pop();

console.log(findLast([1, 2, 3, 4], n => n % 2 === 1));

export const findLastIndex = (arr, fn) =>
  arr
    .map((val, i) => [i, val])
    .filter(([i, val]) => fn(val, i, arr))
    .pop()[0];

console.log(findLastIndex([1, 2, 3, 4], n => n % 2 === 1));
