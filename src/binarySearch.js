const binarySearch = (arr, value) => {
  let start = 0;
  let stop = arr.length - 1;
  while (start <= stop) {
    const mid = Math.floor((start + stop) / 2);
    if (arr[mid] > value) {
      stop = mid - 1;
    } else {
      start = mid + 1;
    }
    if (arr[mid] === value) {
      return mid;
    }
  }
  return -1;
};

export default binarySearch;
