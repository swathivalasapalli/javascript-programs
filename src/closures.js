function makeadder(a) {
  return function(b) {
    return a + b;
  };
}

export default makeadder;
