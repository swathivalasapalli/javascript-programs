import insertionSort from './insertionSort';

test('insertionSort', () => {
  expect(insertionSort([1, 3, 2, 5, 4, 6])).toEqual([1, 2, 3, 4, 5, 6]);
});
