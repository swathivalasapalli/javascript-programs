const repeatTwice = arr => {
  const result = [];
  for (let i = 0; i < arr.length; i += 1) {
    result.push([arr[i], arr[i]]);
  }
  return result;
};

export default repeatTwice;
