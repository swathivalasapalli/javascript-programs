import squareEvenIndicesValue from './squareEvenIndicesValue';

test('squareEvenIndicesValue', () => {
  expect(squareEvenIndicesValue([1, 2, 3, 4, 5])).toEqual([1, 9, 25]);
  expect(squareEvenIndicesValue([1, 2, 3, 4])).toEqual([1, 9]);
});
