const insertionSort = arr => {
  for (let i = 1; i < arr.length; i += 1) {
    const temp = arr[i];
    let j = i - 1;
    while (j >= 0 && arr[j] > temp) {
      arr[j + 1] = arr[j];
      j -= 1;
    }
    arr[j + 1] = temp;
  }
  return arr;
};

export default insertionSort;
