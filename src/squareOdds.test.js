import squareOdds from './squareOdds';

test('squareOdds', () => {
  expect(squareOdds([1, 3, 2, 4, 5])).toEqual([1, 9, 2, 4, 25]);
  expect(squareOdds([1, 9, 2, 4, 5])).toEqual([1, 81, 2, 4, 25]);
});
