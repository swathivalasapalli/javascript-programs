import sortArrayDescending from './sortArrayDescending';

test('sortArrayDescending', () => {
  expect(sortArrayDescending([1, 2, 3, 4])).toEqual([4, 3, 2, 1]);
  expect(sortArrayDescending([2, 1, 3, 4, 8, 7, 9, 6])).toEqual([
    9,
    8,
    7,
    6,
    4,
    3,
    2,
    1,
  ]);
});
