import partition from './partition';

test('partition', () => {
  expect(partition([1, 2, 5, 3, 4, 6, 8], 5)).toEqual([[1, 2, 3, 4], [6, 8]]);
  expect(partition([1, 2, 5, 3, 4, 6], 5)).toEqual([[1, 2, 3, 4], [6]]);
});
