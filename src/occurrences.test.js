import occurrences from './occurrences';

test('occurrences', () => {
  expect(occurrences([1, 2, 3, 4, 5], 6)).toEqual(0);
  expect(occurrences([1, 2, 3, 4, 5, 1, 6, 1], 6)).toEqual(1);
  expect(occurrences([1, 2, 3, 4, 5, 1, 6, 1, 6, 4, 5], 5)).toEqual(2);
  expect(occurrences([1, 1, 1, 2, 3, 4, 5], 1)).toEqual(3);
  expect(occurrences([1, 2, 3, 4, 5, 5, 5, 5, 6, 6, 6, 7, 7, 6], 6)).toEqual(4);
  expect(occurrences([1, 7, 2, 3, 4, 5, 5], 2)).toEqual(1);
  expect(occurrences([1, 1, 1, 1, 1, 2, 2, 2, 2, 3, 4, 5], 2)).toEqual(4);
});
