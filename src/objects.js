const makePerson = (first, last) => ({
  first,
  last,
  fullName() {
    return `${this.first} ${this.last}`;
  },
  fullNameReversed() {
    return `${this.last}, ${this.first}`;
  },
});

export const personFullName = person => `${person.first} ${person.last}`;
export const personFullNameReversed = person =>
  `${person.last}, ${person.first}`;

export const person = (first, last) => {
  this.first = first;
  this.last = last;
  this.fullName = personFullName;
  this.fullNameReversed = personFullNameReversed;
};

export default makePerson;
