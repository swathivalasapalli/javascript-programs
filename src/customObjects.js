const makePerson = (first, last) => ({
  first,
  last,
});
export const personFullName = person => `${person.first} ${person.last}`;
export const personFullNameReversed = person =>
  `${person.last}, ${person.first}`;

export default makePerson;
