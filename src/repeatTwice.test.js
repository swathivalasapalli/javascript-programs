import repeatTwice from './repeatTwice';

test('repeatTwice', () => {
  expect(repeatTwice([1, 2, 3, 4])).toEqual([[1, 1], [2, 2], [3, 3], [4, 4]]);
  expect(repeatTwice([1, 2])).toEqual([[1, 1], [2, 2]]);
});
