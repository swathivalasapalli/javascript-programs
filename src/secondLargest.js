const secondLargest = arr => {
  let max1 = arr[0];
  let max2 = arr[0];
  for (let i = 1; i < arr.length; i += 1) {
    if (arr[i] > max1) {
      max2 = max1;
      max1 = arr[i];
    } else if (arr[i] > max2 && arr[i] !== max1) {
      max2 = arr[i];
    }
  }
  return max2;
};

export default secondLargest;
