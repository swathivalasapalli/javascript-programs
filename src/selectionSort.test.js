import selectionSort from './selectionSort';

test('selectionSort', () => {
  expect(selectionSort([2, 1, 3, 5, 4, 6, 8, 10])).toEqual([
    1,
    2,
    3,
    4,
    5,
    6,
    8,
    10,
  ]);
});
