import secondLargest from './secondLargest';

test('secondLargest', () => {
  expect(secondLargest([1, 1, 2, 3, 3])).toEqual(2);
  expect(secondLargest([1, 1, 2, 2, 3, 3])).toEqual(2);
  expect(secondLargest([1, 2, 3, 4])).toEqual(3);
});
