import pairs from './pairs';

test('pairs', () => {
  expect(pairs([1, 2, 3, 4, 5])).toEqual([[1, 2], [2, 3], [3, 4], [4, 5]]);
  expect(pairs([1, 2, 3])).toEqual([[1, 2], [2, 3]]);
});
