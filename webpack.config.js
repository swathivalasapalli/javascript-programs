const path = require('path');

module.exports = {
  mode: 'development',
  entry: './src/index.js',
  devtool: 'sourcemap',
  output: {
    publicPath: '/build/',
    path: path.resolve(__dirname, 'build'),
    filename: 'bundle.js',
  },
  module: {
    rules: [
      { test: /\.jsx?$/, loader: 'babel-loader' },
      { test: /\.jsx?$/, loader: 'eslint-loader' },
    ],
  },
  resolve: {
    extensions: ['.js', '.jsx'],
    alias: {
      utils$: path.resolve(__dirname, './src/utils.js'),
    },
  },
  devServer: {
    open: true,
    openPage: 'index.html',
    inline: true,
    noInfo: true,
    port: 3000,
  },
};
