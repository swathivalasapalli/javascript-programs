module.exports = {
  parserOptions: {
    ecmaVersion: 2017,
    sourceType: 'module',
  },
  plugins: ['prettier'],
  env: { jest: true, browser: true, es6: true },
  extends: ['airbnb-base', 'prettier'],
  rules: {
    'no-prototype-builtins': 'off',
    'linebreak-style': 'off',
    'no-restricted-syntax': 'off',
    'prettier/prettier': ['error', { singleQuote: true }],
    quotes: ['error', 'single'],
    'arrow-parens': ['warn', 'as-needed'],
    'no-console': 'off',
    'no-await-in-loop': 'off',
    'no-param-reassign': 'off',
    'import/no-extraneous-dependencies': [
      'error',
      { devDependencies: ['**/*.test.js'] },
    ],
  },
};
